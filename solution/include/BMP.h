//
// Created by DmitriiDenisov on 21.09.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "FileOperations.h"
#include "Image.h"
#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include <stdbool.h>


enum errorBMP {
    EB_GOOD_BMP,
    EB_HEADER_ERROR,
    EB_READ_ERROR,
    EB_MEMORY_ERROR,
    EB_WRITE_HEADER_ERROR,
    EB_WRITE_MAP_ERROR,
    EB_WRITE_GOOD
};
const char* getErrorMessageBMP(enum errorBMP errorBMP);

struct __attribute__((packed)) BMPHeader;

struct BMPImage;

enum errorBMP getErrorBMPEnum(struct BMPImage* image);

struct BMPImage* readBMP(struct fileStatusSize* fileStatusSize);
enum errorBMP BMPOut(struct fileStatusSize* fileStatusSize, struct BMPImage* image);

void freeBMP(struct BMPImage* image);
void freeBMPWithOutImage(struct BMPImage* image);

struct image getImageFromBmp(struct BMPImage* bmpImage);
struct BMPImage* setImageToBmp(struct image image);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
