//
// Created by DmitriiDenisov on 25.09.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ANALIZEINPUT_H
#define ASSIGNMENT_IMAGE_ROTATION_ANALIZEINPUT_H

#include <stdbool.h>
#include "TypeImages.h"
enum error {
    EI_COUNT,
    EI_INPUT_EXT,
    EI_OUT_EXT,
    EI_NOT_COMPARE,
    EI_GOOD
};

const char* getErrorMessage(enum error error);

struct returnInput{
    enum error error;
    enum type type;
};

bool checkInput(int argc, char** argv);
struct returnInput giveType(int argc, char** argv);
const char* getFilenameExt(const char *filename);
#endif //ASSIGNMENT_IMAGE_ROTATION_ANALIZEINPUT_H
