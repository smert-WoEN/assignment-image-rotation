//
// Created by DmitriiDenisov on 25.09.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_TYPEIMAGES_H
#define ASSIGNMENT_IMAGE_ROTATION_TYPEIMAGES_H

#include "StringExtend.h"
static const size_t count = 1;
enum type {
    BMP,
    //JPG,

    NONE
};
static const char* typeLabel[] = {
        "bmp"//,
        //"jpg"
};

const char* errorMessageType(void);
enum type valueOf(const char* arg);
#endif //ASSIGNMENT_IMAGE_ROTATION_TYPEIMAGES_H
