//
// Created by DmitriiDenisov on 01.10.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_STRINGEXTEND_H
#define ASSIGNMENT_IMAGE_ROTATION_STRINGEXTEND_H

#include <malloc.h>
#include <string.h>

struct string {
    char* string;
    size_t length;
    size_t capacity;
};
struct string createStringFromStringExtend(void);
void clearStringFromStringExtend(struct string string);
void appendToStringFromStringExtend(struct string* string, char* stringNew);
const char* getStringFromStringExtend(struct string* string);
#endif //ASSIGNMENT_IMAGE_ROTATION_STRINGEXTEND_H
