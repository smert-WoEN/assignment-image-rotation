//
// Created by DmitriiDenisov on 08.10.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include <malloc.h>
struct size {
    uint64_t widthPX;
    uint64_t heightPX;
};


struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

struct image {
    struct size size;
    struct pixel* pixels;
};


struct image rotate90degLeft(struct image imageOld);

void freeImage(struct image image);
struct pixel* getPixelAdr(struct image image, int64_t x, int64_t y);
struct pixel getPixel(struct image image, int64_t x, int64_t y);
#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
