//
// Created by DmitriiDenisov on 02.01.2023.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILEOPERATION_H
#define ASSIGNMENT_IMAGE_ROTATION_FILEOPERATION_H
#include <stdio.h>
#include <stdint.h>
#include <sys/stat.h>
#include <malloc.h>

enum statusFile {
    SF_OPEN,
    SF_ERROR_OPEN,
    SF_ERROR_CLOSE,
    SF_CLOSE
};
enum paramsOpenFile {
    PF_OPEN_RB,
    PF_OPEN_WB
};

struct fileStatusSize;

enum statusFile getStatusFile(struct fileStatusSize* fileStatusSize);

const char* getMessageStatusFile(enum statusFile statusFile);

FILE* getFile(struct fileStatusSize* fileStatusSize);

struct fileStatusSize* openFile(const char* fileName, enum paramsOpenFile paramsOpenFile);

enum statusFile closeFile(struct fileStatusSize* fileStatusSize);

int64_t getFileSizeFromName(const char* fileName);
int64_t getFileSize(struct fileStatusSize* fileStatusSize);
void freeFileStatusSize(struct fileStatusSize* fileStatusSize);
#endif //ASSIGNMENT_IMAGE_ROTATION_FILEOPERATION_H
