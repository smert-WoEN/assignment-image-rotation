//
// Created by DmitriiDenisov on 01.10.2022.
//


#include "StringExtend.h"

struct string createStringFromStringExtend(void) {
   struct string string = {malloc(sizeof(char)), 1, 0};
   string.string[0] = 0;
    return string;
}

void clearStringFromStringExtend(struct string string) {
    if (string.string != NULL) {
        free(string.string);
        string.length = 0;
        string.capacity = 0;
    }
}

void appendToStringFromStringExtend(struct string* string, char* stringNew) {
    size_t length = strlen(stringNew);
    if (length + string -> capacity > string->length - 1) {
        if (string->length + length > length * 2) string->string = realloc(string->string, sizeof(char) * ((string->length) + length));
        else string->string = realloc(string->string, sizeof(char) * (2 * length));
        string->length += length;
    }
    for (size_t i = 0; i < length; ++i) {
        string -> string[string -> capacity + i] = stringNew[i];
    }
    //strcat(string -> string, stringNew);
    string->capacity += length;
    string -> string[string -> capacity] = 0;
}

const char* getStringFromStringExtend(struct string* string) {
    if (string -> capacity == 0) {
        return NULL;
    }
    return string -> string;
}
