//
// Created by DmitriiDenisov on 25.09.2022.
//
#include "TypeImages.h"

enum type valueOf(const char* arg) {
    for (size_t i = 0; i < count; ++i) {
        if (strcmp(arg, typeLabel[i]) == 0) {
            return i;
        }
    }
    return NONE;
}

const char* errorMessageType(void) {
    struct string string = createStringFromStringExtend();
    appendToStringFromStringExtend(&string, "u can use this file. U can use only this formats:\n");
    //char* label = "u can use this file. U can use only this formats:\n";
    for (size_t i = 0; i < count; ++i) {
        appendToStringFromStringExtend(&string, (char *) typeLabel[i]);
        appendToStringFromStringExtend(&string, "\n");
    }
    return getStringFromStringExtend(&string);
}
