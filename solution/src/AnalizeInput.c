//
// Created by DmitriiDenisov on 25.09.2022.
//

#include "AnalizeInput.h"

static const char* errorMessage[] = {
        "u don't input out or input file.",
        "input file ext not found",
        "output file ext not found",
        "files type not compare"
        //"jpg"
};

const char* getErrorMessage(enum error error) {
    if (error == EI_GOOD) return "";
    return errorMessage[error];
}
bool checkInput(int argc, char** argv) {
    (void) argv;
    return argc == 3;
}

struct returnInput giveType(int argc, char** argv){
    if (!checkInput(argc, argv)) return (struct returnInput) {EI_COUNT, NONE};
    const char* image_in_name = getFilenameExt(argv[1]);
    if (strcmp(image_in_name, "") == 0) return (struct returnInput) {EI_INPUT_EXT, NONE};
    enum type image_in = valueOf(image_in_name);
    const char* image_out_name = getFilenameExt(argv[2]);
    if (strcmp(image_out_name, "") == 0) return (struct returnInput) {EI_OUT_EXT, NONE};
    enum type image_out = valueOf(image_out_name);
    if (image_in != image_out) return (struct returnInput) {EI_NOT_COMPARE, NONE};
    return (struct returnInput) {EI_GOOD, image_in};
}

const char* getFilenameExt(const char* filename) {
    char* dot = strrchr(filename, '.');
    if (!dot || dot == filename) return "";
    return dot + 1;
}
