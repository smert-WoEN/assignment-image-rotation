//
// Created by DmitriiDenisov on 02.01.2023.
//
#include "FileOperations.h"

static const char* statusFileMessage[] = {
    "file open correct",
    "file error open",
    "file error close",
    "file close correct"
};

static const char* paramsOpenFileText[] = {
        "rb",
        "wb"
};
struct fileStatusSize {
    FILE* file;
    enum statusFile statusFile;
    int64_t fileSize;
};

enum statusFile getStatusFile(struct fileStatusSize* fileStatusSize) {
    return fileStatusSize -> statusFile;
}

const char* getMessageStatusFile(enum statusFile statusFile) {
    return statusFileMessage[statusFile];
}

FILE* getFile(struct fileStatusSize* fileStatusSize) {
    return fileStatusSize -> file;
}

struct fileStatusSize* openFile(const char* fileName, enum  paramsOpenFile paramsOpenFile) {
    FILE* file = fopen(fileName, paramsOpenFileText[paramsOpenFile]);
    struct fileStatusSize* fileStatusSize = malloc(sizeof(struct fileStatusSize));
    if (file) {
        fileStatusSize->file = file;
        fileStatusSize->statusFile = SF_OPEN;
        fileStatusSize->fileSize = getFileSizeFromName(fileName);
    } else {
        fileStatusSize->file = NULL;
        fileStatusSize->statusFile = SF_ERROR_OPEN;
        fileStatusSize->fileSize = 0;
    }
    return fileStatusSize;
}

enum statusFile closeFile(struct fileStatusSize* fileStatusSize) {
    enum statusFile statusFile;
    if (fileStatusSize -> statusFile == SF_OPEN && fclose(fileStatusSize -> file) != EOF) statusFile = SF_CLOSE;
    else statusFile = SF_ERROR_CLOSE;
    freeFileStatusSize(fileStatusSize);
    return statusFile;
}

int64_t getFileSizeFromName(const char* fileName) {
    struct stat st;
    stat(fileName, &st);
    return st.st_size;
}

int64_t getFileSize(struct fileStatusSize* fileStatusSize) {
    return fileStatusSize->fileSize;
}

void freeFileStatusSize(struct fileStatusSize* fileStatusSize) {
    if (fileStatusSize) free(fileStatusSize);
}



