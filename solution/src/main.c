#include "AnalizeInput.h"
#include "BMP.h"
#include "FileOperations.h"
#include "Image.h"
#include <stdio.h>

int main(int argc, char** argv) {
    (void) argc; (void) argv; // supress 'unused parameters' warningt
    struct returnInput format = giveType(argc, argv);
    if (format.error != EI_GOOD || format.type == NONE) {
        fprintf(stderr, "%s\n", format.error != EI_GOOD ? getErrorMessage(format.error) : errorMessageType());
        return 0;
    }

    struct fileStatusSize* fileStatusSize = openFile(argv[1], PF_OPEN_RB);
    if (getStatusFile(fileStatusSize) != SF_OPEN) {
        fprintf(stderr, "%s\n", getMessageStatusFile(getStatusFile(fileStatusSize)));
        freeFileStatusSize(fileStatusSize);
        return 0;
    }
    fprintf(stdout, "%s\n", getMessageStatusFile(getStatusFile(fileStatusSize)));

    struct image image;
    switch (format.type) {
        case BMP: {
            struct BMPImage* bmpImage = readBMP(fileStatusSize);
            if (getErrorBMPEnum(bmpImage) != EB_GOOD_BMP){
                fprintf(stderr, "%s\n", getErrorMessageBMP(getErrorBMPEnum(bmpImage)));
                freeBMP(bmpImage);
                return 0;
            }
            fprintf(stdout, "%s\n", getErrorMessageBMP(getErrorBMPEnum(bmpImage)));
            image = getImageFromBmp(bmpImage);
            freeBMPWithOutImage(bmpImage);
            break;
        }
//        case JPG: {
//
//        }
        default: {
            fprintf(stderr, "%s\n", "some error");
            return 0;
        }
    }
    struct image newImage = rotate90degLeft(image);
    freeImage(image);

    enum statusFile statusFile = closeFile(fileStatusSize);
    if (statusFile != SF_CLOSE) {
        fprintf(stderr, "%s\n", getMessageStatusFile(statusFile));
        return 0;
    }
    fprintf(stdout, "%s\n", getMessageStatusFile(statusFile));

    struct fileStatusSize* fileStatusSize2 = openFile(argv[2], PF_OPEN_WB);
    if (getStatusFile(fileStatusSize2) != SF_OPEN) {
        fprintf(stderr, "%s\n", getMessageStatusFile(getStatusFile(fileStatusSize2)));
        freeFileStatusSize(fileStatusSize2);
        return 0;
    }
    fprintf(stdout, "%s\n", getMessageStatusFile(getStatusFile(fileStatusSize2)));

    switch (format.type) {
        case BMP: {
            struct BMPImage* bmpImage1 = setImageToBmp(newImage);
            enum errorBMP errorBmp = BMPOut(fileStatusSize2, bmpImage1);
            fprintf(errorBmp != EB_WRITE_GOOD ? stderr : stdout, "%s\n", getErrorMessageBMP(errorBmp));
            freeBMP(bmpImage1);
            break;
        }
//        case JPG: {
//
//        }
        default: {
            fprintf(stderr, "%s\n", "some error");
            return 0;
        }
    }

    enum statusFile statusFile2 = closeFile(fileStatusSize2);
    if (statusFile2 != SF_CLOSE) {
        fprintf(stderr, "%s\n", getMessageStatusFile(statusFile));
        return 0;
    }
    fprintf(stdout, "%s\n", getMessageStatusFile(statusFile));

    return 0;
}
