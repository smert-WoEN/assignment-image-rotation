//
// Created by DmitriiDenisov on 21.09.2022.
//
#include "BMP.h"


static const uint16_t magicValue = 0x4D42;
static const uint32_t bmpHeaderSize = 54;
static const uint32_t dipHeaderSize = 40;
static const uint16_t numPlanes = 1;
static const uint32_t compression = 0;
static const uint32_t numColors = 0;
static const uint32_t importantColors = 0;
static const uint16_t bitsPerPixel = 24;
static const uint16_t bitsPerByte = 8;

static const char* errorMessageBmp[] = {
        "bmr read complete",
        "header damage, check u bmp file",
        "read error, try again or change file",
        "memory error, try again or reloading PC",
        "write header to file error",
        "write array to file error",
        "bmp write complete"
        //"jpg"
};

const char* getErrorMessageBMP(enum errorBMP errorBMP) {
    return errorMessageBmp[errorBMP];
}

struct __attribute__((packed)) BMPHeader{
    uint16_t type;
    uint32_t size;
    uint16_t reserved1;
    uint16_t reserved2;
    uint32_t offset;
    uint32_t dibHeaderSize;
    int32_t  widthPX;
    int32_t  heightPX;
    uint16_t numPlanes;
    uint16_t bitsPerPixel;
    uint32_t compression;
    uint32_t imageSizeBytes;
    int32_t  xResolutionPPM;
    int32_t  yResolutionPPM;
    uint32_t numColors;
    uint32_t importantColors;
};

struct BMPImage {
    enum errorBMP errorBmp;
    struct image image;
};

enum errorBMP getErrorBMPEnum(struct BMPImage* image) {
    return image->errorBmp;
}

uint64_t getBytePerPixel(struct BMPHeader bmpHeader) {
    return bmpHeader.bitsPerPixel / bitsPerByte;
}

uint64_t getPadding(struct BMPHeader bmpHeader) {
    return (4 - (bmpHeader.widthPX * getBytePerPixel(bmpHeader)) % 4) % 4;
}

uint64_t getImageRowSizeBites(struct BMPHeader bmpHeader) {
    return bmpHeader.widthPX * getBytePerPixel(bmpHeader) + getPadding(bmpHeader);
}

uint64_t getImageSizeBytes(struct  BMPHeader bmpHeader) {
    return getImageRowSizeBites(bmpHeader) * bmpHeader.heightPX;
}

uint64_t getImageRowSizeBitesWithOutPadding(struct BMPHeader bmpHeader) {
    return bmpHeader.widthPX * getBytePerPixel(bmpHeader);
}

uint64_t getImageSizeBytesWithOutPadding(struct  BMPHeader bmpHeader) {
    return getImageRowSizeBitesWithOutPadding(bmpHeader) * bmpHeader.heightPX;
}

bool checkBMPHeader(struct BMPHeader bmpHeader, int64_t fileSize) {
    return bmpHeader.type == magicValue &&
           bmpHeader.offset >= bmpHeaderSize &&
           bmpHeader.dibHeaderSize == dipHeaderSize &&
           bmpHeader.numPlanes == numPlanes &&
           bmpHeader.compression == compression &&
           bmpHeader.numColors == numColors &&
           bmpHeader.importantColors == importantColors &&
           bmpHeader.bitsPerPixel == bitsPerPixel &&
           bmpHeader.size == fileSize;// &&
    //bmpHeader -> imageSizeBytes == getImageSizeBytes(bmpHeader);
}

struct BMPImage* readBMP(struct fileStatusSize* fileStatusSize){
    struct BMPImage* image = malloc(sizeof(struct BMPImage)); //malloc(sizeof(struct BMPImage));
    struct BMPHeader bmpHeader = {0};
    FILE* file = getFile(fileStatusSize);
    size_t numRead = fread(&bmpHeader, //sizeof(image -> BMPHeader),
                             sizeof(bmpHeader), 1, file);
    if (numRead != 1) {
        image->errorBmp = EB_READ_ERROR;
        return image;
    }
    if (!checkBMPHeader(bmpHeader, getFileSize(fileStatusSize))) {
        image->errorBmp = EB_HEADER_ERROR;
        return image;
    }
    if (fseek(file, (long) bmpHeader.offset, SEEK_SET) != 0) {
        image->errorBmp = EB_HEADER_ERROR;
        return image;
    }
    image->image.pixels = malloc(getImageSizeBytesWithOutPadding(bmpHeader));
    image->image.size.heightPX=bmpHeader.heightPX;
    image->image.size.widthPX=bmpHeader.widthPX;
    if (image->image.pixels == NULL)
    {
        image->errorBmp = EB_MEMORY_ERROR;
        return image;
    }
    for (size_t i = 0; i < bmpHeader.heightPX; ++i) {
        for (size_t j = 0; j < getImageRowSizeBitesWithOutPadding(bmpHeader) / 3; ++j) {
            size_t numRead2 = fread(getPixelAdr(image->image, (int64_t) j, (int64_t) i), getBytePerPixel(bmpHeader) /*image -> BMPHeader.imageSizeBytes*/, 1, file);
            if (numRead2 != 1)
            {
                image->errorBmp = EB_READ_ERROR;
                return image;
            }
        }
        if (fseek(file, (long) getPadding(bmpHeader), SEEK_CUR) != 0) {
            image->errorBmp = EB_HEADER_ERROR;
            return image;
        }
    }
    image -> errorBmp = EB_GOOD_BMP;
    return image;
}

void freeBMP(struct BMPImage* image) {
    if (image) {
        if (image->image.pixels != NULL) free(image->image.pixels);
        free(image);
    }
}

void freeBMPWithOutImage(struct BMPImage* image) {
    if (image) {
        free(image);
    }
}


struct BMPHeader generateBmpHeader(struct size* size) {
    struct BMPHeader bmpHeader = {0};
    bmpHeader.type = magicValue;
    bmpHeader.offset = bmpHeaderSize;
    bmpHeader.dibHeaderSize = dipHeaderSize;
    bmpHeader.widthPX = (int32_t) size -> widthPX;
    bmpHeader.heightPX = (int32_t) size -> heightPX;
    bmpHeader.numPlanes = numPlanes;
    bmpHeader.bitsPerPixel = bitsPerPixel;
    bmpHeader.compression = compression;
    bmpHeader.numColors = numColors;
    bmpHeader.importantColors = importantColors;
    bmpHeader.imageSizeBytes = getImageSizeBytes(bmpHeader);
    bmpHeader.size = bmpHeader.imageSizeBytes + bmpHeaderSize;
    return bmpHeader;
}

enum errorBMP BMPOut(struct fileStatusSize* fileStatusSize, struct BMPImage* image) {
    FILE* file = getFile(fileStatusSize);
    if (file != NULL && image != NULL){
        struct BMPHeader bmpHeader = generateBmpHeader(&image->image.size);
        size_t numRead = fwrite(&bmpHeader, sizeof(bmpHeader), 1, file);
        //(void) numRead;
        if (numRead != 1) {
            return EB_WRITE_HEADER_ERROR;
        }
        for (size_t i = 0; i < bmpHeader.heightPX; ++i) {
            for (size_t j = 0; j < getImageRowSizeBitesWithOutPadding(bmpHeader) / 3; ++j) {
                size_t numRead1 = fwrite(getPixelAdr(image->image, (int64_t) j, (int64_t) i), getBytePerPixel(bmpHeader), 1, file);
                if (numRead1 != 1) {
                    return EB_WRITE_MAP_ERROR;
                }
            }
            uint64_t padding = getPadding(bmpHeader);
            for (size_t j = 0; j < padding; ++j) {
                size_t numRead1 = fwrite("0", sizeof(uint8_t), 1, file);
                if (numRead1 != 1) {
                    return EB_WRITE_MAP_ERROR;
                }
            }
        }
        return EB_WRITE_GOOD;
    }
    return EB_WRITE_HEADER_ERROR;
}

struct image getImageFromBmp(struct BMPImage* bmpImage) {
    return bmpImage->image;
}

struct BMPImage* setImageToBmp(struct image image) {
    struct BMPImage* bmpImage2 = malloc(sizeof(struct BMPImage));
    bmpImage2->image = image;
    return bmpImage2;
}
