//
// Created by DmitriiDenisov on 08.10.2022.
//

#include "Image.h"

struct image rotate90degLeft(struct image imageOld) {
    struct image image;// = malloc(sizeof(struct image));
    image.size = (struct size) {imageOld.size.heightPX, imageOld.size.widthPX};
    image.pixels = malloc(sizeof(struct pixel) * image.size.heightPX * image.size.widthPX);
    for (size_t i = 0; i < image.size.heightPX; ++i) {
        size_t size = sizeof(struct pixel) * image.size.widthPX;
        if (size > 0) {
            //image.pixels[i] = malloc(size);
            for (size_t j = 0; j < image.size.widthPX; ++j) {
                image.pixels[i * image.size.widthPX + j] = getPixel(imageOld, (int64_t) i, (int64_t) (imageOld.size.heightPX - j - 1));
            }
        }
    }
    return image;
}

void freeImage(struct image image) {
    if (image.pixels != NULL) {
        free(image.pixels);
    }
}

struct pixel* getPixelAdr(struct image image, int64_t x, int64_t y) {
    return &image.pixels[x + y * image.size.widthPX];
}

struct pixel getPixel(struct image image, int64_t x, int64_t y) {
    return image.pixels[x + y * image.size.widthPX];
}

